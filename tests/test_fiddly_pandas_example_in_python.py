from string import ascii_letters

import numpy as np
import pandas as pd

import hyfive.curried as hf

N_ROWS = 100

NAME_REGISTRY = pd.DataFrame({
    'resident_id': np.arange(N_ROWS),
    'first_name': np.random.choice(list(ascii_letters), N_ROWS),
    'last_name': np.random.choice(list(ascii_letters), N_ROWS)
})

AGE_REGISTRY = pd.DataFrame({
    'resident_id': np.arange(N_ROWS),
    'age': np.random.uniform(1, 80, N_ROWS)
})

def create_dataframe():
    mod_res_id = NAME_REGISTRY.resident_id % 3
    variant = np.where(mod_res_id == 1, 'a', np.where(mod_res_id == 2, 'b', 'c'))
    select_ix = np.isin(variant, ['a', 'b'])
    dataframe = (NAME_REGISTRY
                 .assign(variant=variant)
                 [select_ix]
                 .merge(AGE_REGISTRY, on='resident_id')
                 .groupby('variant')
                 .apply(lambda df: pd.Series({
                     'min_age': np.min(df.age),
                     'mean_age': np.mean(df.age),
                     'std_age': np.std(df.age),
                     'max_age': np.max(df.age)
                 }))
                 .reset_index()
                 .sort_values(by='min_age', ascending=False)
                 .reset_index(drop=True))
    return dataframe


def create_dataframe_with_thread():
    mod_res_id = hf.mod('resident_id', 3)
    return hf.thread(
        NAME_REGISTRY,
        hf.with_column(
            'variant',
            hf.cond_col([hf.is_eq(mod_res_id, 1), hf.lit('a')],
                        [hf.is_eq(mod_res_id, 2), hf.lit('b')],
                        [True                   , hf.lit('c')])
        ),
        hf.filter(hf.is_in('variant', ['a', 'b'])),
        hf.join(AGE_REGISTRY, on='resident_id'),
        hf.group_by('variant'),
        hf.agg({
                "min_age":  hf.min("age"),
                "mean_age": hf.mean("age"),
                "std_age":  hf.std("age"),
                "max_age":  hf.max("age"),
        }),
        hf.order_by('min_age', desc=True)
    )


def test_fiddly_pandas_example_in_python():
    dataframe = create_dataframe()
    assert dataframe.shape == (2, 5), "DataFrame does have the expected shape."
    assert (
        set(dataframe.columns) == 
        {'variant', 'min_age', 'mean_age', 'std_age', 'max_age'}
    ), 'DataFrame does not have the expected columns.'
    first, second, *_ = dataframe.min_age.tolist()
    assert second < first, 'DataFrame should be sorted by min_age.'


def test_thread():
    dataframe = create_dataframe()
    dataframe_with_thread = create_dataframe_with_thread()
    pd.testing.assert_frame_equal(dataframe, dataframe_with_thread)
