(require [hy.contrib.walk [let]])

(import [numpy :as np])
(import [pandas :as pd])
(import [pytest :as pt])

(import [hyfive :as hf])
(import [hyfive.utils [$ lmap]])


(setv DATAFRAME (pd.DataFrame {"a" ["a" "b" "c" "d" "e"]
                               "b" [2 3 5 7 11]
                               "c" [1 2 3 4 5]}))


(defn test-run-col []
  (let [column (hf.col "a")]
    (assert (= (.tolist (hf.run-col DATAFRAME column))
               (.tolist DATAFRAME.a))
            "hf.run-col does not work properly on col + string."))
  (let [column (hf.lit "a")]
    (assert (= (.tolist (hf.run-col DATAFRAME column))
               (* ["a"] (len DATAFRAME)))
            "hf.run-col does not work properly on lit + string.")))


(defn test-column []
  (let [column (hf.col "b")]
    (assert (hf.col? column) "Wrong definition of const column.")
    (assert (= (.tolist (hf.run-col DATAFRAME column))
               (.tolist DATAFRAME.b))
            "Running const column does not give the expected result."))
  (let [column (hf.add "b" "c")
        applied (hf.run-col DATAFRAME column)]
    (assert (hf.col? column) "Wrong definition of composite column.")
    (assert (= (.tolist applied) (.tolist (+ DATAFRAME.b DATAFRAME.c)))
            "Running composite column does not give the expected result.")))


(defn test-mod []
  (let [column (hf.mod "b" 3)
        result (hf.run-col DATAFRAME column)]
    (assert (= (.tolist result) [2 0 2 1 2])
            "hf.mod does not return expected result.")))


(defn test-if-col []
  (let [column (hf.if-col (hf.eq? "a" (hf.lit "d")) "b" "c")
        result (hf.run-col DATAFRAME column)]
    (assert (= (.tolist result) [1 2 3 7 5])
            "hf.if-col does not return expected result.")))


(defn test-cond-col []
  (let [c-mod-3 (hf.mod "c" 3)
        column  (hf.cond-col [(hf.eq? c-mod-3 0) (hf.mul "c" 10)]
                             [(hf.eq? c-mod-3 1) (hf.mul "c" 100)]
                             [:else              (hf.mul "c" 1000)])
        result (hf.run-col DATAFRAME column)]
    (assert (= (.tolist result) [100 2000 30 400 5000])
            "hf.cond-col does not return expected result for complete case."))
  (let [column (hf.cond-col [(hf.eq? (hf.col "c") 2) "c"])
        result (hf.run-col DATAFRAME column)]
    (assert (np.allclose (.tolist result)
                         [np.nan 2 np.nan np.nan np.nan]
                         :equal-nan True)
            "hf.cond-col does not return expected result for incomplete case.")))


(defn test-column-alias []
  (let [column (hf.mod "a" 10)]
    (assert (= (hf.alias column) "(mod a 10)")
            "Wrong alias for hf.mod.")
    (assert (= (hf.alias (hf.as column "abcdef")) "abcdef")
            "hf.as does not rename alias correctly."))
  (let [column (hf.eq? (hf.mul "b" 123) (hf.add "c" -10))]
    (assert (= (hf.alias column) "(eq? (mul b 123) (add c -10))")
            "Wrong alias for hf.eq? + hf.mul."))
  (let [column (hf.if-col (hf.eq? "b" "c") (hf.lit 1) (hf.lit 0))]
    (assert (= (hf.alias column) "(if (eq? b c) 1 0)")
            "Wrong alias for hf.if-col.")))


(defn test-is-in []
  (let [column (hf.is-in "a" ["a" "c" "e"])
        result (hf.run-col DATAFRAME column)]
    (assert (= (.tolist result) [True False True False True])
            "hf.is-in does not return expected result.")))


(defn test-agg-fns []
  (let [agg-fns    [hf.min hf.mean hf.std hf.max]
        agg-values (lmap
                     (fn [agg-fn] (hf.run-col DATAFRAME (agg-fn "b")))
                     agg-fns)
        expected   (lmap ($ DATAFRAME.b) [np.min np.mean np.std np.max])]
    (assert (np.allclose agg-values expected)
            "Column agg functions do not reconcile with numpy's functions.")))
