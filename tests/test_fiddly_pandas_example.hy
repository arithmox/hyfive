(require [hy.contrib.walk [let]])

(import [string [ascii-letters]])

(import [numpy :as np])
(import [pandas :as pd])
(import [pytest :as pt])

(import [hyfive :as hf])

(setv N-ROWS 100)


(setv NAME-REGISTRY
  (pd.DataFrame {"resident_id" (np.arange N-ROWS)
                 "first_name"  (np.random.choice (list ascii-letters) N-ROWS)
                 "last_name"   (np.random.choice (list ascii-letters) N-ROWS)}))


(setv AGE-REGISTRY
  (pd.DataFrame {"resident_id" (np.arange N-ROWS)
                 "age"         (np.random.uniform 1 80 N-ROWS)}))


(setv DATAFRAME
  (-> NAME-REGISTRY
      (hf.with-column "variant"
        (let [mod-res-id (hf.mod "resident_id" 3)]
          (hf.cond-col [(hf.eq? mod-res-id 1) (hf.lit "a")]
                       [(hf.eq? mod-res-id 2) (hf.lit "b")]
                       [:else                 (hf.lit "c")])))
      (hf.filter (hf.is-in "variant" ["a" "b"]))
      (hf.join AGE-REGISTRY :on "resident_id")
      (hf.group-by "variant")
      (hf.agg {"min_age"  (hf.min "age")
               "mean_age" (hf.mean "age")
               "std_age"  (hf.std "age")
               "max_age"  (hf.max "age")})
      (hf.order-by "min_age" :desc True)))


(defn test-fiddly-pandas-example []
  (assert (= DATAFRAME.shape (, 2 5))
          "DataFrame does have the expected shape.")
  (assert (= (set DATAFRAME.columns)
             #{"variant" "min_age" "mean_age" "std_age" "max_age"})
          "DataFrame does not have the expected columns.")
  (assert (= (set (.tolist DATAFRAME.variant))
             #{"a" "b"})
          "All c variants should be filtered out.")
  (let [ages (.tolist DATAFRAME.min-age)]
    (assert (> (first ages) (second ages))
            "DataFrame should be sorted by min-age.")))
