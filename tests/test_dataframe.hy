(require [hy.contrib.walk [let]])

(import [numpy :as np])
(import [pandas :as pd])
(import [pytest :as pt])

(import [hyfive :as hf])


(setv DATAFRAME (pd.DataFrame {"a" [1 2 3 4 5]
                               "b" [1 2 3 2 1]
                               "c" [5 4 3 2 1]}))


(defn test-select []
  (let [selected (hf.select DATAFRAME "a")]
    (assert (= ((. selected columns tolist)) ["a"])
            "hf.select selects wrong column: a."))
  (let [selected (hf.select DATAFRAME "b" "c")]
    (assert (= ((. selected columns tolist) ["b" "d"]))
            "hf.select selects wrong column: b and c."))
  (with [(pt.raises KeyError
           :message "hf.select missing column should fail.")]
    (hf.select DATAFRAME "c" "d"))
  (let [selected (hf.select DATAFRAME (hf.as (hf.mul "b" 2) "x")
                                      (hf.as (hf.mod "c" 2) "y"))]
    (assert (= (set selected.columns) #{"x" "y"})
            "hf.select has wrong columns with HyFive cols.")
    (assert (and (= (.tolist selected.x) [2 4 6 4 2])
                 (= (.tolist selected.y) [1 0 1 0 1]))
            "hf.select has wrong values with HyFive cols."))
  (with [(pt.raises KeyError
           :message "hf.select should fail with mixed col type.")]
    (hf.select DATAFRAME "a" (hf.col "b"))))


(defn test-filter []
  (let [filtered (hf.filter DATAFRAME (fn [x] (!= (% x.a 2) 0)))]
    (assert (np.all (= filtered.index (np.arange (len filtered))))
            "hf.filter should reset the Dataframe index.")
    (assert (= (filtered.a.tolist) [1 3 5])
            "hf.filter filtered out the wrong rows of own column.")
    (assert (= (filtered.b.tolist) [1 3 1])
            "hf.filter filtered out the wrong rows of other column."))
  (let [filtered (hf.filter DATAFRAME (hf.eq? "b" 2))]
    (assert (= (filtered.a.tolist) [2 4])
            "hf.filter filtered out the wrong rows with HyFive column.")))


(defn test-with-column []
  (let [col-added (hf.with-column DATAFRAME "d" (fn [x] (+ x.a x.b)))]
    (assert (= ((. col-added d tolist)) [2 4 6 6 6])
            "hf.with-column does not insert the right column."))
  (let [col-added (hf.with-column DATAFRAME "a" (fn [x] (** x.a 2)))]
    (assert (= ((. col-added a tolist)) [1 4 9 16 25])
            "hf.with-column does not replace column correctly.")
    (assert (= ((. DATAFRAME a tolist)) [1 2 3 4 5])
            "hf.with-column should not mutate the original DataFrame.")))


(defn test-with-columns []
  (let [cols-added (hf.with-columns DATAFRAME {"a" (fn [x] 1)
                                               "d" (fn [x] (+ x.a x.b))})]
    (assert (= ((. cols-added a tolist)) [1 1 1 1 1])
            "hf.with-columns does not replace existing col.")
    (assert (= ((. cols-added d tolist)) [2 4 6 6 6])
            "hf.with-columns does not change new col."))

  (let [cols-added (hf.with-columns DATAFRAME {"a" (hf.mul 2 "a")
                                               "d" (hf.mod 9 "b")})]
    (assert (= (.tolist cols-added.a) [2 4 6 8 10])
            "hf.with-columns does not replace existing col with HyFive columns.")
    (assert (= (.tolist cols-added.d) [0 1 0 1 0])
            "hf.with-columns does not change new col with HyFive columns.")))


(defn test-with-column-renamed []
  (let [renamed (hf.with-column-renamed DATAFRAME "a" "c")]
    (with [(pt.raises KeyError
             :message "renamed column should no longer be selectable.")]
      (hf.select renamed "a"))
    (assert (= (set renamed) #{"c" "b"})
            "hf.rename should drop target column and preserve other columns.")
    (assert (= ((. renamed c tolist))
               ((. DATAFRAME a tolist)))
            "hf.rename should not mutate original values."))
  (let [renamed (hf.with-column-renamed DATAFRAME "b" "d")]
    (assert (= (set renamed) #{"a" "c" "d"})
            "hf.rename should drop target column and add the new column.")
    (assert (= ((. renamed d tolist))
               ((. DATAFRAME b tolist)))
            "hf.rename should not mutate original values.")))


(defn test_with_columns_renamed []
  (let [renamed (hf.with-columns-renamed DATAFRAME {"a" "A"
                                                    "b" "B"
                                                    "c" "C"})]
    (assert (= (set renamed) #{"A" "B" "C"})
            "hf.with-columns-renamed do not rename correctly.")
    (assert (= (set DATAFRAME) #{"a" "b" "c"})
            "hf.with-columns-renamed mutated the original DataFrame.")))


(defn test-join []
  (let [other-df (pd.DataFrame {"a" [5 2 3 4 1] "d" [5 4 3 2 1]})
        joined (hf.join DATAFRAME other-df :on ["a"] :how "inner")]
    (assert (= ((. joined d tolist)) [1 4 3 2 5])
            "hf.join inner is not joining correctly."))
  (let [other-df (pd.DataFrame {"x" [2 3 5 7 9] "y" [5 4 3 2 1]})
        joined (hf.join DATAFRAME other-df :left-on ["a"]
                                           :right-on ["y"]
                                           :how "inner")]
    (assert (= ((. joined x tolist)) [9 7 5 3 2])
            "Passing kwargs to hf.join does not work properly.")))


(defn test-with-column []
  (let [col-added (hf.with-column DATAFRAME "d" (fn [x] (+ x.a x.b)))]
    (assert (= ((. col-added d tolist)) [2 4 6 6 6])
            "hf.with-column does not insert the right column."))
  (let [col-added (hf.with-column DATAFRAME "a" (fn [x] (** x.a 2)))]
    (assert (= ((. col-added a tolist)) [1 4 9 16 25])
            "hf.with-column does not replace column correctly.")
    (assert (= ((. DATAFRAME a tolist)) [1 2 3 4 5])
            "hf.with-column should not mutate the original DataFrame."))
  (let [col-added (hf.with-column DATAFRAME "x" (hf.mul "a" "b"))]
    (assert (= (.tolist col-added.x) [1 4 9 8 5])
            "hf.with-column does not work with HyFive column.")))


(defn test-drop-columns []
  (assert (not (in "b" (hf.drop-columns DATAFRAME "b")))
          "hf.drop-columns does not actually drop column.")
  (let [dropped (hf.drop-columns DATAFRAME "a"  "b")]
    (assert (= (set dropped) #{"c"})
            "hf.drop-columns does not drop multiple columns properly.")))


(defn test-groupby-agg []
  (let [aggregated (-> DATAFRAME
                       (hf.group-by "b")
                       (hf.agg {"sum_a" (fn [x] (.sum x.a))
                                "std_a" (fn [x] (.std x.a))
                                "min_c" (hf.min "c")
                                "max_c" (hf.max "c")}))]
    (assert (= (set aggregated.columns)
               #{"b" "sum_a" "std_a" "min_c" "max_c"})
            "hf.group-by + hf.agg do not produce the expected columns."))
  (let [aggregated (-> DATAFRAME
                       (hf.group-by "b")
                       (hf.agg (hf.sum "a")
                               (hf.mean "c"))
                       (hf.order-by "b"))]
    (assert (and (= (.tolist (get aggregated "(sum a)") [6 6 3]))
                 (= (.tolist (get aggregated "(mean c)") [3 3 3])))
            (+ "hf.group-by + hf.agg does not produce expected "
               "results with HyFive columns.")))
  (let [aggregated (-> DATAFRAME
                       (hf.with-column "d" [1 2 2 2 1])
                       (hf.group-by "b" "d")
                       (hf.agg (hf.sum "a"))
                       (hf.order-by "b"))]
    (assert (and (= (set aggregated) #{"b" "d" "(sum a)"})
                 (= (.tolist (get aggregated "(sum a)") [6 6 3])))
            (+ "variadic hf.group-by + hf.agg does not produce "
               "expected results with HyFive columns."))))


(defn test-order-by []
  (let [ordered (hf.order-by DATAFRAME "b")]
    (assert (and (= (.tolist ordered.b) [1 1 2 2 3])
                 (= (.tolist ordered.a) [1 5 2 4 3]))
            "hf.order-by does not order correctly."))
  (let [ordered (hf.order-by DATAFRAME "a" :desc True)]
    (assert (and (= (.tolist ordered.c) [1 2 3 4 5])
                 (= (.tolist ordered.a) [5 4 3 2 1]))
            "hf.order-by with desc does not order correctly.")))
